package com.example.apigetdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Arrays;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    String[] menuItem;
    ArrayList<ArrayList<String>> menuSubItems = new ArrayList<>();
    RecyclerView rv;
    CustomAdapter cus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        DataInterface dataInterface = GetInstance.getRetrofitInstance().create(DataInterface.class);
        Call<com.example.apigetdemo.Response> call = dataInterface.getParameter();

        call.enqueue(new Callback<com.example.apigetdemo.Response>() {
            @Override

            public void onResponse(Call<com.example.apigetdemo.Response> call, Response<com.example.apigetdemo.Response> response) {
                assert response.body() != null;

                String menuItemsStr = response.body().getData().getPackageDetails().getPackageParameter();
                menuItem = menuItemsStr.split(",");

                for (String s : menuItem) {

                    ArrayList<String> parameterList = new ArrayList<>(Arrays.asList(s.split("\\$")));
                    menuSubItems.add(parameterList);

                }
                cus.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<com.example.apigetdemo.Response> call, Throwable t) {
                t.printStackTrace();
            }
        });
        rv = findViewById(R.id.rv);
        cus = new CustomAdapter(this, menuSubItems);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(MainActivity.this);
        rv.setLayoutManager(manager);
        rv.setAdapter(cus);


    }
}