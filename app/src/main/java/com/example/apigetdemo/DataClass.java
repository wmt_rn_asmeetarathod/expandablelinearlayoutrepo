package com.example.apigetdemo;

import com.google.gson.annotations.SerializedName;

public class DataClass {

    @SerializedName("data")
    public String data;

   /* @SerializedName("package_details")
    public String packageDetails;

    @SerializedName("package_parameter")
    public String parameter;*/

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

  /*  public String getPackageDetails() {
        return packageDetails;
    }

    public void setPackageDetails(String packageDetails) {
        this.packageDetails = packageDetails;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public DataClass(String data, String packageDetails, String parameter) {
        this.data = data;
        this.packageDetails = packageDetails;
        this.parameter = parameter;
    }*/

    public DataClass(String data) {
        this.data = data;
    }
}
