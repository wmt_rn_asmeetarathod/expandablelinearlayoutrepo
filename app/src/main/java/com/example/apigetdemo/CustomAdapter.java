package com.example.apigetdemo;

import android.annotation.SuppressLint;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.aakira.expandablelayout.ExpandableLinearLayout;


import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.customViewHolder> {

    private final ArrayList<ArrayList<String>> menuSubItems;
    private final Context context;


    public CustomAdapter(Context context, ArrayList<ArrayList<String>> menuSubItems) {
        this.menuSubItems = menuSubItems;
        this.context = context;
    }

    @NonNull
    @Override
    public customViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_layout, parent, false);
        return new customViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull customViewHolder holder, int position) {
        ArrayList<String> s = menuSubItems.get(position);
        StringBuilder sb = new StringBuilder();

        if (s.size() > 1) {

            int len = s.size() - 1;
            holder.tvItems.setText(s.get(0) + " (" + len + ")");

            for (int i = 1; i < s.size(); i++) {

                String s1 = s.get(i);
                sb.append("\n").append(s1).append("\n");
                holder.tvSubItems.setText(sb);

            }

        } else {
            holder.tvItems.setText(s.get(0));
        }


        holder.layout.setInRecyclerView(true);
        holder.tvItems.setOnClickListener(v -> {

            if (holder.layout.getVisibility() == View.GONE) {

                holder.layout.setVisibility(View.VISIBLE);
                holder.layout.initLayout();
                holder.layout.expand();
                holder.tvItems.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_baseline_expand_less_24, 0);

            } else if (holder.layout.getVisibility() == View.VISIBLE) {

                holder.layout.setVisibility(View.GONE);
                holder.layout.collapse();
                holder.tvItems.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_baseline_expand_more_24, 0);
            }


        });
    }


    @Override
    public int getItemCount() {
        return menuSubItems.size();
    }


    static class customViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        TextView tvItems, tvSubItems;
        public ExpandableLinearLayout layout;

        public customViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            tvItems = view.findViewById(R.id.tvItems);
            tvSubItems = view.findViewById(R.id.tvSubItems);
            layout = view.findViewById(R.id.expandLayout);

        }
    }
}
