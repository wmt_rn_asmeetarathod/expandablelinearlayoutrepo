package com.example.apigetdemo;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PackageDetails{

	@SerializedName("lims_package_code")
	private String limsPackageCode;

	@SerializedName("original_price")
	private int originalPrice;

	@SerializedName("lims_package_flag")
	private String limsPackageFlag;

	@SerializedName("habit_id")
	private String habitId;

	@SerializedName("description")
	private String description;

	@SerializedName("pre_test_information")
	private PreTestInformation preTestInformation;

	@SerializedName("package_parameter")
	private String packageParameter;

	@SerializedName("report_available_time")
	private String reportAvailableTime;

	@SerializedName("risk_area_id")
	private String riskAreaId;

	@SerializedName("package_test")
	private List<Object> packageTest;

	@SerializedName("disease_id")
	private Object diseaseId;

	@SerializedName("image_path")
	private String imagePath;

	@SerializedName("discount_percentage_value")
	private int discountPercentageValue;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("common_package")
	private int commonPackage;

	@SerializedName("status")
	private int status;

	public void setLimsPackageCode(String limsPackageCode){
		this.limsPackageCode = limsPackageCode;
	}

	public String getLimsPackageCode(){
		return limsPackageCode;
	}

	public void setOriginalPrice(int originalPrice){
		this.originalPrice = originalPrice;
	}

	public int getOriginalPrice(){
		return originalPrice;
	}

	public void setLimsPackageFlag(String limsPackageFlag){
		this.limsPackageFlag = limsPackageFlag;
	}

	public String getLimsPackageFlag(){
		return limsPackageFlag;
	}

	public void setHabitId(String habitId){
		this.habitId = habitId;
	}

	public String getHabitId(){
		return habitId;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setPreTestInformation(PreTestInformation preTestInformation){
		this.preTestInformation = preTestInformation;
	}

	public PreTestInformation getPreTestInformation(){
		return preTestInformation;
	}

	public void setPackageParameter(String packageParameter){
		this.packageParameter = packageParameter;
	}

	public String getPackageParameter(){
		return packageParameter;
	}

	public void setReportAvailableTime(String reportAvailableTime){
		this.reportAvailableTime = reportAvailableTime;
	}

	public String getReportAvailableTime(){
		return reportAvailableTime;
	}

	public void setRiskAreaId(String riskAreaId){
		this.riskAreaId = riskAreaId;
	}

	public String getRiskAreaId(){
		return riskAreaId;
	}

	public void setPackageTest(List<Object> packageTest){
		this.packageTest = packageTest;
	}

	public List<Object> getPackageTest(){
		return packageTest;
	}

	public void setDiseaseId(Object diseaseId){
		this.diseaseId = diseaseId;
	}

	public Object getDiseaseId(){
		return diseaseId;
	}

	public void setImagePath(String imagePath){
		this.imagePath = imagePath;
	}

	public String getImagePath(){
		return imagePath;
	}

	public void setDiscountPercentageValue(int discountPercentageValue){
		this.discountPercentageValue = discountPercentageValue;
	}

	public int getDiscountPercentageValue(){
		return discountPercentageValue;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCommonPackage(int commonPackage){
		this.commonPackage = commonPackage;
	}

	public int getCommonPackage(){
		return commonPackage;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"PackageDetails{" + 
			"lims_package_code = '" + limsPackageCode + '\'' + 
			",original_price = '" + originalPrice + '\'' + 
			",lims_package_flag = '" + limsPackageFlag + '\'' + 
			",habit_id = '" + habitId + '\'' + 
			",description = '" + description + '\'' + 
			",pre_test_information = '" + preTestInformation + '\'' + 
			",package_parameter = '" + packageParameter + '\'' + 
			",report_available_time = '" + reportAvailableTime + '\'' + 
			",risk_area_id = '" + riskAreaId + '\'' + 
			",package_test = '" + packageTest + '\'' + 
			",disease_id = '" + diseaseId + '\'' + 
			",image_path = '" + imagePath + '\'' + 
			",discount_percentage_value = '" + discountPercentageValue + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",common_package = '" + commonPackage + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}