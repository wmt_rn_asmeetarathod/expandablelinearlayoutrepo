package com.example.apigetdemo;


import retrofit2.Call;
import retrofit2.http.GET;

public interface DataInterface {

    @GET("/api/v1/users/getPackage/1")
    Call<Response> getParameter();

}
