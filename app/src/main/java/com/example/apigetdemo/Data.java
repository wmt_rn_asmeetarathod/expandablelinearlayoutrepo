package com.example.apigetdemo;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("package_details")
	private PackageDetails packageDetails;

	public void setPackageDetails(PackageDetails packageDetails){
		this.packageDetails = packageDetails;
	}

	public PackageDetails getPackageDetails(){
		return packageDetails;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"package_details = '" + packageDetails + '\'' + 
			"}";
		}
}